# 3088F microHAT PROJECT

Designing an uninterrupted power supply unit for a microHat

**PROJECT DESCRIPTION**

This project is aimed at creating an uninterrupted power supply(UPS) that will power up a micro-hat during load shedding.
The UPS can either power the hat for the entire duration of the load shedding or for a few minutes of safe shutdown.
Status LEDs are used to indicate the battery power supply of the UPS available for supplying the PiHat.
Colour of LEDs on indicate the battery percentage: green-full, blue-intermediate and red-critical.


**HOW THE LED COLOUR CODING WORKS **

3 LEDs; green LED, blue LED and red LED will are used to indicate the battery suply percentage.

If all 3 LEDs are on, the battery supply is full and can supply the hat for longer.

As the battery gets used up, the total power available for the hat to use decreases.

If the battery decreases below 80% of full power state, the green LED turns off, leaving only the blue and red LEDs on.

If the battery decreases below 50% of full poer state, the blue LED switches off, leaving only the red LED on.

If the battery decreases below 20% of full power state, the hat will shut down and the red LED will switch off, leaving none on.
